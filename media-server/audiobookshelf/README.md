# Make sure to add an .env file
example `.env` file
```
# Time zone
TIME_ZONE=

# Tailscale
TS_AUTHKEY=
```