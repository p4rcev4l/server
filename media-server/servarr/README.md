# Make sure to add an .env file
example `.env` file
```
# General settings
TIME_ZONE=
PUID=
PGID=

# Wireguard settings
VPN_PROVIDER=
VPN_LAN_NETWORK=

# Unpackerr settings
RADARR_URL=
RADARR_API_KEY=
SONARR_URL=
SONARR_API_KEY=
READARR_URL=
READARR_API_KEY=
```