# Make sure to add an .env file
example `.env` file
```
# Tailscale
TS_AUTHKEY=

###################################################################################

# You can find documentation for all the supported env variables at https://immich.app/docs/install/environment-variables

# The location where your uploaded files are stored
UPLOAD_LOCATION=

# The Immich version to use. You can pin this to a specific version like "v1.71.0"
IMMICH_VERSION=release

# Connection secrets for postgres and typesense. You should change these to random passwords
DB_PASSWORD=

# Needed for reverse proxy
#IMMICH_SERVER_URL=
#IMMICH_WEB_URL=

# The values below this line do not need to be changed
###################################################################################
DB_HOSTNAME=immich_postgres
DB_USERNAME=postgres
DB_DATABASE_NAME=immich
DB_DATA_LOCATION=pgdata

REDIS_HOSTNAME=immich_redis
```